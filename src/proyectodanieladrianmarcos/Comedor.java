package proyectodanieladrianmarcos;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

/**
 *
 * @author DAM124
 */
public class Comedor 
{
    private LinkedList<Mesa> mesas;

    public Comedor(LinkedList<Mesa> mesas) 
    {
        this.mesas = mesas;
    }
    
    public void añadir(Mesa nueva)
    {
        mesas.add(nueva);
    }
    public Mesa posicionMesa(int n)
    {
        return mesas.get(n-1);
    }
    public boolean empty()
    {
        if(this.mesas.isEmpty())
            return true;
        return false;
    }
    public void guardar()
    {
        File archivo= new File("restaurante.dat");
        FileOutputStream fos = null;
        DataOutputStream dos = null;
        try {
            fos = new FileOutputStream(archivo, false);
            dos = new DataOutputStream(new BufferedOutputStream(fos));
            for (Mesa p : mesas) {
                dos.writeUTF(p.getNombre());
                dos.writeInt(p.getNumeroAsientos());
                dos.writeBoolean(p.isEstado());
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Fichero no encontrado");
        } catch (IOException ex) {
            System.out.println("Excepción de E/S");
        } finally {
            try {
                if (dos != null) {
                    dos.close();
                }
                if (fos != null) {
                    fos.close();
                }

            } catch (IOException ex) {
                System.out.println("Error al cerrar el fichero");
            }
        }
    }
    
    public void cargar()
    {
        DataInputStream dis = null;
        File fichero = new File("restaurante.dat");
        int i=0;
        if (fichero.exists()) 
        {
            try {
                dis = new DataInputStream(
                        new BufferedInputStream(
                                new FileInputStream(fichero)));

                while (true) {
                    String nombre = dis.readUTF();
                    int numAsi = dis.readInt();
                    boolean est = dis.readBoolean();
                    this.mesas.get(i).setEstado(est);
                    if(i<3)
                    i++;
                }

            } catch (FileNotFoundException ex) {
                System.out.println("Fichero no encontrado");
            } catch (EOFException eofe) {
                //System.out.println("Fin de fichero");
            } catch (IOException ex) {
                System.out.println("Excepción de E/S");
            } finally {
                if (dis != null) {
                    try {
                        dis.close();
                    } catch (IOException ex) {
                        System.out.println("Error al cerrar");
                    }
                }
            }
        }
    }
    public LinkedList<Mesa> cargarComedor(){
    return this.mesas;
    }
}

