/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectodanieladrianmarcos;

/**
 *
 * @author Daniel
 */
public class Comanda {
    private int cantidad;
    private Platos plato;

public Comanda(int cantidad, Platos plato) {
        this.cantidad = cantidad;
        this.plato = plato;
    }

    @Override
    public String toString() {
        return  cantidad + "x " + plato.getNombre() + " ("+plato.getPrecio()*cantidad+")€";
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Platos getPlato() {
        return plato;
    }

    public void setPlato(Platos plato) {
        this.plato = plato;
    }


}