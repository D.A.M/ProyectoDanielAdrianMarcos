/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectodanieladrianmarcos;

/**
 *
 * @author usuario
 */
public class Platos {
    private int numero;
    private String nombre;
    private double precio;

    public Platos(String nombre, double precio, int numero) {
        this.nombre = nombre;
        this.precio = precio;
        this.numero=numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return  nombre + " (" + precio + "€)";
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
}
