/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectodanieladrianmarcos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.DefaultListModel;

/**
 *
 * @author DAM126
 */
public class MetodosBBDD {
    /**
     * 
     * @param List Introducir la lista anteriormente inicializada
     * @param tipo introducir el numero equivalente al tipo de comida 0 Otros 1 Primeros 2 Segundos
     */
    public static void cargarPlatos(DefaultListModel List,int tipo) {
        Platos p;
        Connection con = BaseDatos.getInstance().getConnection();
        String sql = "Select nombre,precio,ID from menu where Tipo="+tipo+";";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                p = new Platos(rs.getString(1), rs.getDouble(2),rs.getInt(3));
                List.addElement(p);
            }
            
            rs.close();
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public static void registrarFactura(int nfact,int nplato,int cantidad) {
        Connection con = BaseDatos.getInstance().getConnection();
        String sql = "INSERT INTO facturas values (?,?,?,curdate());";
        try {

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setInt(1,nfact );
            ps.setInt(2,nplato );
            ps.setInt(3,cantidad );
            ps.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Insert mal hecho");
        }
    }
    public static int ultimafactura(){
        int numero=0;
        Connection con = BaseDatos.getInstance().getConnection();
        String sql = "Select n_factura from facturas order by n_factura desc limit 1";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
               numero=rs.getInt(1);
                 }
            
            rs.close();
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return numero;
    }
}
