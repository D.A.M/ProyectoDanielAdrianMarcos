package proyectodanieladrianmarcos;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.DefaultListModel;

/**
 *
 * @author DAM124
 */
public class Mesa implements Serializable {

    private String nombre;
    private int numeroAsientos;
    private boolean estado;
    //
    private LinkedList<Comanda> c;
    private DefaultListModel<Comanda> modeloTotal;

    public Mesa(String nombre, int numeroAsientos, boolean estado) {
        this.nombre = nombre;
        this.numeroAsientos = numeroAsientos;
        this.estado = estado;
        modeloTotal = new DefaultListModel<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumeroAsientos() {
        return numeroAsientos;
    }

    public void setNumeroAsientos(int numeroAsientos) {
        this.numeroAsientos = numeroAsientos;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public void añadircomandas(Comanda comanda) {
        modeloTotal.addElement(comanda);
    }

    @Override
    public String toString() {
        return nombre;
    }

    public DefaultListModel<Comanda> getModeloTotal() {
        return modeloTotal;
    }

    public void setModeloTotal(DefaultListModel<Comanda> modeloTotal) {
        this.modeloTotal = modeloTotal;
    }
    //No funciona
    /*
    public void cargarC(){
    for(int i=0;i<modeloTotal.size();i++){
    this.c.add(modeloTotal.elementAt(i));
        System.out.println(modeloTotal.getElementAt(i));
    }
    }
    public void borrarcomanda(Comanda c) {
        cargarC();
        Iterator<Comanda> it = this.c.iterator();
        while (it.hasNext()) {
            Comanda p = it.next();
            if (p == c) {
                it.remove();
            }
            this.modeloTotal = null;
            for (Comanda co : this.c) {
                this.modeloTotal.addElement(co);
            }
        }
    }*/

}
