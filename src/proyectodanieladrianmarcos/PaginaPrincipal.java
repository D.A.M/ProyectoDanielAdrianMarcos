/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectodanieladrianmarcos;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

/**
 *
 * @author DAM107
 */
public class PaginaPrincipal extends javax.swing.JFrame {

    private LinkedList<Mesa> mesas = new LinkedList();
    private Comedor comedor = new Comedor(mesas);
    private DefaultListModel<Platos> modeloPrimeros = new DefaultListModel<>();
    private DefaultListModel<Platos> modeloSegundos = new DefaultListModel<>();
    private DefaultListModel<Platos> modeloOtros = new DefaultListModel<>();
    private DefaultListModel<Comanda> modeloTotal = new DefaultListModel<>();
    private Platos p;

    public PaginaPrincipal() {
        initComponents();
        //imagenes principal
        ImageIcon fondo = new ImageIcon("ImagenesProyecto/fondoladrillos.jpg");
        Icon icono = new ImageIcon(fondo.getImage().getScaledInstance(PPrincipal.getWidth(), PPrincipal.getHeight(), Image.SCALE_DEFAULT));
        ImageIcon imgcoci = new ImageIcon("ImagenesProyecto/cocinero.png");
        Icon iconcoci = new ImageIcon(imgcoci.getImage().getScaledInstance(Coci.getWidth(), Coci.getHeight(), Image.SCALE_DEFAULT));
        Fondo.setIcon(icono);
        Coci.setIcon(iconcoci);
        ImageIcon imgcom = new ImageIcon("ImagenesProyecto/restaurante.jpg");
        Icon iconcom = new ImageIcon(imgcom.getImage().getScaledInstance(FondoComandas.getWidth(), FondoComandas.getHeight(), Image.SCALE_DEFAULT));
        FondoComandas.setIcon(iconcom);
        this.repaint();
        //imagenes botones
        ImageIcon cubiertos = new ImageIcon("ImagenesProyecto/cubiertos.png");
        Icon cub = new ImageIcon(cubiertos.getImage().getScaledInstance(BotonCubiertos.getWidth() - 2, BotonCubiertos.getHeight() - 2, Image.SCALE_DEFAULT));
        ImageIcon mesa = new ImageIcon("ImagenesProyecto/mesa.png");
        Icon mes = new ImageIcon(mesa.getImage().getScaledInstance(BotonComedor.getWidth() - 10, BotonComedor.getHeight() - 2, Image.SCALE_DEFAULT));
        BotonComedor.setIcon(mes);
        BotonCubiertos.setIcon(cub);
        //cargar imagenes para el panel del comedor
        ImageIcon fondo1 = new ImageIcon("ImagenesProyecto/restaurante.jpg");
        Icon icono2 = new ImageIcon(fondo1.getImage().getScaledInstance(Fondo2.getWidth(), Fondo2.getHeight(), Image.SCALE_DEFAULT));
        Fondo2.setIcon(icono2);

        if (comedor.empty()) {
            Mesa m1 = new Mesa("Mesa: 1", 2, true);
            Mesa m2 = new Mesa("Mesa: 2", 2, true);
            Mesa m3 = new Mesa("Mesa: 3", 4, true);
            Mesa m4 = new Mesa("Mesa: 4", 6, true);
            comedor.añadir(m1);
            comedor.añadir(m2);
            comedor.añadir(m3);
            comedor.añadir(m4);

        }
        if (new File("restaurante.dat").exists()) {
            comedor.cargar();
            cargarColor(Mesa1, CheckMesa1, comedor.posicionMesa(1));
            cargarColor(Mesa2, CheckMesa2, comedor.posicionMesa(2));
            cargarColor(Mesa3, CheckMesa3, comedor.posicionMesa(3));
            cargarColor(Mesa4, CheckMesa4, comedor.posicionMesa(4));
        } else {

            cambiaColor(Mesa1, CheckMesa1, comedor.posicionMesa(1));
            cambiaColor(Mesa2, CheckMesa2, comedor.posicionMesa(2));
            cambiaColor(Mesa3, CheckMesa3, comedor.posicionMesa(3));
            cambiaColor(Mesa4, CheckMesa4, comedor.posicionMesa(4));
            // prueba();
        }
        MetodosBBDD.cargarPlatos(modeloOtros, 0);
        MetodosBBDD.cargarPlatos(modeloPrimeros, 1);
        MetodosBBDD.cargarPlatos(modeloSegundos, 2);
        jListOtros.setModel(modeloOtros);
        jListPrimeros.setModel(modeloPrimeros);
        jListSegundos.setModel(modeloSegundos);
        jListTotal.setModel(modeloTotal);
        mostrarPrecio(PrecioLabel);
    }

    private void cambiaColor(JLabel mesa, JCheckBox check, Mesa m) {
        if (check.isSelected()) {
            mesa.setBackground(Color.red);
            mesa.setText("Ocupado");
            mesa.setForeground(Color.green);
            m.setEstado(false);
        } else if (check.isSelected() == false) {
            mesa.setBackground(Color.green);
            mesa.setText("Libre");
            mesa.setForeground(Color.red);
            m.setEstado(true);
        }
    }

    private void cargarColor(JLabel mesa, JCheckBox check, Mesa m) {
        if (!m.isEstado()) {
            mesa.setBackground(Color.red);
            mesa.setText("Ocupado");
            mesa.setForeground(Color.green);
            check.setSelected(true);
        } else if (m.isEstado()) {
            mesa.setBackground(Color.green);
            mesa.setText("Libre");
            mesa.setForeground(Color.red);
            check.setSelected(false);
        }
    }

    private Mesa cambiarmesa(String nomb) {
        Mesa d = null;
        for (Mesa m : comedor.cargarComedor()) {
            if (m.getNombre().equals(nomb)) {
                modeloTotal = m.getModeloTotal();
                d = m;
            }
        }
        return d;
    }

    private void cargarComanda(Platos p, Mesa m) {
        if (p != null && (int) jSpinner1.getValue() > 0) {
            Comanda c = new Comanda((int) jSpinner1.getValue(), p);
            m.añadircomandas(c);
            mostrarPrecio(PrecioLabel);
        }

    }

    private void hacerFactura() {
        File f = new File("factura.txt");
        FileWriter fr = null;
        BufferedWriter br = null;
        int numero=MetodosBBDD.ultimafactura()+1;
        try {
            if (f.exists()) {
                f.delete();
            }
            fr = new FileWriter(f);
            br = new BufferedWriter(fr);
            br.write("Restaurante DANIADRIANMARCOS");
            br.newLine();
            br.flush();
            br.write("------------------------------");
            br.newLine();
            br.flush();
            for (int i = 0; i < modeloTotal.size(); i++) {
                String linea = modeloTotal.getElementAt(i).toString();

                br.write(linea);
                br.newLine();
                br.flush();
                MetodosBBDD.registrarFactura(numero,modeloTotal.getElementAt(i).getPlato().getNumero() ,modeloTotal.getElementAt(i).getCantidad());
            }
            br.write("------------------------------");
            br.newLine();
            br.flush();
            br.write("Total--------------------- " + Precio() + "€");
            br.newLine();
            br.flush();
            
        } catch (IOException ex) {
            System.out.println("Error de E/S");
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    System.out.println("Error al cerrar el flujo");
                }
            }
            System.out.println("Factura Hecha");
        }
    }

    private int cogernumero(String s) {
        int i = 0;
        while (s.charAt(i) != 'x') {
            i++;
        }
        return Integer.parseInt(s.substring(0, i));
    }

    private void factura() {
        jLabelNumeroMesa.setText((String) MesasComboBox.getSelectedItem());
        jListTotal.getModel();
        jDialog1.setVisible(true);
        JListaDialog.setModel(modeloTotal);
        mostrarPrecio(jLabelPrecioDialog);

        //hacer un iterator y comparar los platos y hay alguno que es igual ir eliminandolos.
    }

    private int avisoBorrar() {
        return JOptionPane.showConfirmDialog(null, "¿Esta seguro que quiere borrar?", "Advertencia", JOptionPane.YES_NO_OPTION);
    }

    private void borrar() {
        modeloTotal.removeElementAt(jListTotal.getSelectedIndex());
        mostrarPrecio(PrecioLabel);
    }

    private void borrartodo(boolean b) {
        if (b) {
            modeloTotal.removeAllElements();
        } else if (avisoBorrar() == 0) {
            modeloTotal.removeAllElements();
        }
        mostrarPrecio(PrecioLabel);
    }

    private double Precio() {
        double precio = 0;
        for (int i = 0; i < modeloTotal.getSize(); i++) {
            precio += modeloTotal.getElementAt(i).getCantidad() * modeloTotal.getElementAt(i).getPlato().getPrecio();
        }
        return precio;
    }

    private void mostrarPrecio(JLabel j) {

        j.setText("" + Precio() + "€");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cambiarTodojPopupMenu = new javax.swing.JPopupMenu();
        todoOcupadojMenuItem = new javax.swing.JMenuItem();
        todoLibreMenuItem = new javax.swing.JMenuItem();
        jDialog1 = new javax.swing.JDialog();
        jLabelNumeroMesa = new javax.swing.JLabel();
        jLabelPrecioDialog = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        JListaDialog = new javax.swing.JList<>();
        jButtonHacerFactura = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        BotonComedor = new javax.swing.JButton();
        BotonCubiertos = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        PPrincipal = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        Coci = new javax.swing.JLabel();
        Fondo = new javax.swing.JLabel();
        PanelRest = new javax.swing.JPanel();
        PanelMesa1 = new javax.swing.JPanel();
        Mesa1 = new javax.swing.JLabel();
        jButton12 = new javax.swing.JButton();
        CheckMesa1 = new javax.swing.JCheckBox();
        jButton13 = new javax.swing.JButton();
        PanelMesa2 = new javax.swing.JPanel();
        Mesa2 = new javax.swing.JLabel();
        jButton14 = new javax.swing.JButton();
        CheckMesa2 = new javax.swing.JCheckBox();
        jButton15 = new javax.swing.JButton();
        PanelMesa4 = new javax.swing.JPanel();
        Mesa4 = new javax.swing.JLabel();
        jButton18 = new javax.swing.JButton();
        CheckMesa4 = new javax.swing.JCheckBox();
        jButton19 = new javax.swing.JButton();
        PanelMesa3 = new javax.swing.JPanel();
        Mesa3 = new javax.swing.JLabel();
        jButton16 = new javax.swing.JButton();
        CheckMesa3 = new javax.swing.JCheckBox();
        jButton17 = new javax.swing.JButton();
        Fondo2 = new javax.swing.JLabel();
        PanelComandas = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        finalizarjButton = new javax.swing.JButton();
        PrecioLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        aceptarjButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListPrimeros = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListSegundos = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListOtros = new javax.swing.JList<>();
        MesasComboBox = new javax.swing.JComboBox<>();
        jSpinner1 = new javax.swing.JSpinner();
        jScrollPane4 = new javax.swing.JScrollPane();
        jListTotal = new javax.swing.JList<>();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        FondoComandas = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        guardarjMenuItem = new javax.swing.JMenuItem();
        MenuSalida = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        ComedorjMenuItem = new javax.swing.JMenuItem();
        ComandasjMenuItem = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        AcercaDe = new javax.swing.JMenuItem();

        todoOcupadojMenuItem.setText("Todo ocupado");
        todoOcupadojMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                todoOcupadojMenuItemActionPerformed(evt);
            }
        });
        cambiarTodojPopupMenu.add(todoOcupadojMenuItem);

        todoLibreMenuItem.setText("Todo libre");
        todoLibreMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                todoLibreMenuItemActionPerformed(evt);
            }
        });
        cambiarTodojPopupMenu.add(todoLibreMenuItem);

        jDialog1.setMinimumSize(new java.awt.Dimension(450, 350));

        jLabelNumeroMesa.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelNumeroMesa.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabelPrecioDialog.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabelPrecioDialog.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jScrollPane5.setViewportView(JListaDialog);

        jButtonHacerFactura.setText("Hacer Factura");
        jButtonHacerFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHacerFacturaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelNumeroMesa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                            .addComponent(jLabelPrecioDialog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 55, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(97, 97, 97)
                .addComponent(jButtonHacerFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabelNumeroMesa, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonHacerFactura)
                .addGap(10, 10, 10)
                .addComponent(jLabelPrecioDialog, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImages(null);
        setMinimumSize(new java.awt.Dimension(923, 570));
        setResizable(false);
        setSize(new java.awt.Dimension(0, 0));
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setForeground(new java.awt.Color(255, 0, 0));
        jPanel1.setOpaque(false);
        jPanel1.setLayout(null);

        BotonComedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonComedorActionPerformed(evt);
            }
        });
        jPanel1.add(BotonComedor);
        BotonComedor.setBounds(80, 0, 80, 80);

        BotonCubiertos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonCubiertosActionPerformed(evt);
            }
        });
        jPanel1.add(BotonCubiertos);
        BotonCubiertos.setBounds(0, 0, 80, 80);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 920, 78);

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.setPreferredSize(new java.awt.Dimension(920, 440));
        jPanel2.setLayout(new java.awt.CardLayout());

        PPrincipal.setLayout(null);

        jButton1.setText("Ir al Comedor");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        PPrincipal.add(jButton1);
        jButton1.setBounds(340, 280, 218, 71);
        PPrincipal.add(Coci);
        Coci.setBounds(290, 40, 312, 261);
        PPrincipal.add(Fondo);
        Fondo.setBounds(0, -10, 930, 460);

        jPanel2.add(PPrincipal, "card3");

        PanelRest.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PanelRestMouseClicked(evt);
            }
        });
        PanelRest.setLayout(null);

        PanelMesa1.setOpaque(false);

        Mesa1.setBackground(new java.awt.Color(50, 40, 40));
        Mesa1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Mesa1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Mesa1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Mesa1.setOpaque(true);

        jButton12.setForeground(new java.awt.Color(51, 0, 255));

        CheckMesa1.setText("Mesa 1");
        CheckMesa1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckMesa1ActionPerformed(evt);
            }
        });

        jButton13.setForeground(new java.awt.Color(51, 0, 255));

        javax.swing.GroupLayout PanelMesa1Layout = new javax.swing.GroupLayout(PanelMesa1);
        PanelMesa1.setLayout(PanelMesa1Layout);
        PanelMesa1Layout.setHorizontalGroup(
            PanelMesa1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMesa1Layout.createSequentialGroup()
                .addGroup(PanelMesa1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelMesa1Layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(CheckMesa1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Mesa1, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa1Layout.createSequentialGroup()
                        .addGap(207, 207, 207)
                        .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa1Layout.createSequentialGroup()
                        .addGap(205, 205, 205)
                        .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(122, Short.MAX_VALUE))
        );
        PanelMesa1Layout.setVerticalGroup(
            PanelMesa1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelMesa1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(PanelMesa1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelMesa1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Mesa1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa1Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addComponent(CheckMesa1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelRest.add(PanelMesa1);
        PanelMesa1.setBounds(0, 0, 425, 194);

        PanelMesa2.setOpaque(false);

        Mesa2.setBackground(new java.awt.Color(50, 40, 40));
        Mesa2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Mesa2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Mesa2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Mesa2.setOpaque(true);

        jButton14.setForeground(new java.awt.Color(51, 0, 255));

        CheckMesa2.setText("Mesa2");
        CheckMesa2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckMesa2ActionPerformed(evt);
            }
        });

        jButton15.setForeground(new java.awt.Color(51, 0, 255));

        javax.swing.GroupLayout PanelMesa2Layout = new javax.swing.GroupLayout(PanelMesa2);
        PanelMesa2.setLayout(PanelMesa2Layout);
        PanelMesa2Layout.setHorizontalGroup(
            PanelMesa2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMesa2Layout.createSequentialGroup()
                .addGroup(PanelMesa2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelMesa2Layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(CheckMesa2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Mesa2, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa2Layout.createSequentialGroup()
                        .addGap(207, 207, 207)
                        .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa2Layout.createSequentialGroup()
                        .addGap(205, 205, 205)
                        .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(182, Short.MAX_VALUE))
        );
        PanelMesa2Layout.setVerticalGroup(
            PanelMesa2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelMesa2Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(PanelMesa2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelMesa2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Mesa2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa2Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(CheckMesa2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelRest.add(PanelMesa2);
        PanelMesa2.setBounds(435, 0, 483, 194);

        PanelMesa4.setOpaque(false);

        Mesa4.setBackground(new java.awt.Color(50, 40, 40));
        Mesa4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Mesa4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Mesa4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Mesa4.setOpaque(true);

        jButton18.setForeground(new java.awt.Color(51, 0, 255));

        CheckMesa4.setText("Mesa4");
        CheckMesa4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckMesa4ActionPerformed(evt);
            }
        });

        jButton19.setForeground(new java.awt.Color(51, 0, 255));

        javax.swing.GroupLayout PanelMesa4Layout = new javax.swing.GroupLayout(PanelMesa4);
        PanelMesa4.setLayout(PanelMesa4Layout);
        PanelMesa4Layout.setHorizontalGroup(
            PanelMesa4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMesa4Layout.createSequentialGroup()
                .addGroup(PanelMesa4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelMesa4Layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(CheckMesa4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Mesa4, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa4Layout.createSequentialGroup()
                        .addGap(207, 207, 207)
                        .addComponent(jButton18, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa4Layout.createSequentialGroup()
                        .addGap(205, 205, 205)
                        .addComponent(jButton19, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(182, Short.MAX_VALUE))
        );
        PanelMesa4Layout.setVerticalGroup(
            PanelMesa4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelMesa4Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jButton18, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(PanelMesa4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelMesa4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Mesa4, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa4Layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(CheckMesa4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton19, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelRest.add(PanelMesa4);
        PanelMesa4.setBounds(435, 212, 483, 194);

        PanelMesa3.setOpaque(false);

        Mesa3.setBackground(new java.awt.Color(50, 40, 40));
        Mesa3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Mesa3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Mesa3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Mesa3.setOpaque(true);

        jButton16.setForeground(new java.awt.Color(51, 0, 255));

        CheckMesa3.setText("Mesa3");
        CheckMesa3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckMesa3ActionPerformed(evt);
            }
        });

        jButton17.setForeground(new java.awt.Color(51, 0, 255));

        javax.swing.GroupLayout PanelMesa3Layout = new javax.swing.GroupLayout(PanelMesa3);
        PanelMesa3.setLayout(PanelMesa3Layout);
        PanelMesa3Layout.setHorizontalGroup(
            PanelMesa3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelMesa3Layout.createSequentialGroup()
                .addGroup(PanelMesa3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelMesa3Layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(CheckMesa3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Mesa3, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa3Layout.createSequentialGroup()
                        .addGap(207, 207, 207)
                        .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa3Layout.createSequentialGroup()
                        .addGap(205, 205, 205)
                        .addComponent(jButton17, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(124, Short.MAX_VALUE))
        );
        PanelMesa3Layout.setVerticalGroup(
            PanelMesa3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelMesa3Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(PanelMesa3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelMesa3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Mesa3, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelMesa3Layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addComponent(CheckMesa3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton17, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelRest.add(PanelMesa3);
        PanelMesa3.setBounds(0, 212, 425, 194);
        PanelRest.add(Fondo2);
        Fondo2.setBounds(-6, -6, 930, 450);

        jPanel2.add(PanelRest, "card2");

        PanelComandas.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Mesa:");
        PanelComandas.add(jLabel1);
        jLabel1.setBounds(10, 33, 62, 29);

        finalizarjButton.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        finalizarjButton.setText("Finalizar");
        finalizarjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finalizarjButtonActionPerformed(evt);
            }
        });
        PanelComandas.add(finalizarjButton);
        finalizarjButton.setBounds(681, 359, 176, 25);

        PrecioLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        PrecioLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PanelComandas.add(PrecioLabel);
        PrecioLabel.setBounds(365, 273, 203, 111);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Primeros:");
        PanelComandas.add(jLabel2);
        jLabel2.setBounds(202, 36, 88, 22);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Segundos:");
        PanelComandas.add(jLabel3);
        jLabel3.setBounds(469, 36, 95, 22);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Otros:");
        PanelComandas.add(jLabel4);
        jLabel4.setBounds(759, 36, 56, 22);

        aceptarjButton.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        aceptarjButton.setText("Aceptar");
        aceptarjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aceptarjButtonActionPerformed(evt);
            }
        });
        PanelComandas.add(aceptarjButton);
        aceptarjButton.setBounds(350, 230, 356, 25);

        jListPrimeros.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListPrimerosValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jListPrimeros);

        PanelComandas.add(jScrollPane1);
        jScrollPane1.setBounds(168, 68, 164, 130);

        jListSegundos.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListSegundosValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jListSegundos);

        PanelComandas.add(jScrollPane2);
        jScrollPane2.setBounds(419, 68, 195, 130);

        jListOtros.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListOtrosValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(jListOtros);

        PanelComandas.add(jScrollPane3);
        jScrollPane3.setBounds(696, 68, 200, 130);

        MesasComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MesasComboBoxActionPerformed(evt);
            }
        });
        PanelComandas.add(MesasComboBox);
        MesasComboBox.setBounds(10, 68, 86, 20);
        PanelComandas.add(jSpinner1);
        jSpinner1.setBounds(40, 169, 56, 29);

        jScrollPane4.setViewportView(jListTotal);

        PanelComandas.add(jScrollPane4);
        jScrollPane4.setBounds(10, 230, 211, 130);

        jButton2.setText("Borrar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        PanelComandas.add(jButton2);
        jButton2.setBounds(239, 297, 63, 23);

        jButton3.setText("BorrarTodo");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        PanelComandas.add(jButton3);
        jButton3.setBounds(239, 337, 122, 23);
        PanelComandas.add(FondoComandas);
        FondoComandas.setBounds(0, 0, 918, 443);

        jPanel2.add(PanelComandas, "card4");

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 80, 920, 440);

        jMenu1.setText("Archivo");
        jMenu1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                jMenu1ComponentHidden(evt);
            }
        });
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        guardarjMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        guardarjMenuItem.setText("Guardar");
        guardarjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarjMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(guardarjMenuItem);

        MenuSalida.setText("Salir");
        MenuSalida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuSalidaActionPerformed(evt);
            }
        });
        jMenu1.add(MenuSalida);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Gestión");
        jMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu2ActionPerformed(evt);
            }
        });

        ComedorjMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        ComedorjMenuItem.setText("Comedor");
        ComedorjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComedorjMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(ComedorjMenuItem);

        ComandasjMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        ComandasjMenuItem.setText("Comandas");
        ComandasjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComandasjMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(ComandasjMenuItem);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Ayuda");

        AcercaDe.setText("Acerca de...");
        AcercaDe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AcercaDeActionPerformed(evt);
            }
        });
        jMenu3.add(AcercaDe);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenu1ComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jMenu1ComponentHidden
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1ComponentHidden

    private void CheckMesa1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckMesa1ActionPerformed
        cambiaColor(Mesa1, CheckMesa1, comedor.posicionMesa(1));
    }//GEN-LAST:event_CheckMesa1ActionPerformed

    private void CheckMesa3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckMesa3ActionPerformed
        cambiaColor(Mesa3, CheckMesa3, comedor.posicionMesa(3));
    }//GEN-LAST:event_CheckMesa3ActionPerformed

    private void CheckMesa4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckMesa4ActionPerformed
        cambiaColor(Mesa4, CheckMesa4, comedor.posicionMesa(4));
    }//GEN-LAST:event_CheckMesa4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        PPrincipal.setVisible(false);
        PanelRest.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void AcercaDeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AcercaDeActionPerformed
        PantallaMensaje();
    }//GEN-LAST:event_AcercaDeActionPerformed

    private void MenuSalidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuSalidaActionPerformed
        System.exit(0);
    }//GEN-LAST:event_MenuSalidaActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        comedor.guardar();
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void CheckMesa2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckMesa2ActionPerformed
        cambiaColor(Mesa2, CheckMesa2, comedor.posicionMesa(2));
    }//GEN-LAST:event_CheckMesa2ActionPerformed

    private void PanelRestMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PanelRestMouseClicked
        cambiarTodojPopupMenu.show(this, evt.getX(), evt.getY());
    }//GEN-LAST:event_PanelRestMouseClicked

    private void todoOcupadojMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_todoOcupadojMenuItemActionPerformed
        CheckMesa1.setSelected(true);
        CheckMesa2.setSelected(true);
        CheckMesa3.setSelected(true);
        CheckMesa4.setSelected(true);
        cambiaColor(Mesa1, CheckMesa1, comedor.posicionMesa(1));
        cambiaColor(Mesa2, CheckMesa2, comedor.posicionMesa(2));
        cambiaColor(Mesa3, CheckMesa3, comedor.posicionMesa(3));
        cambiaColor(Mesa4, CheckMesa4, comedor.posicionMesa(4));
    }//GEN-LAST:event_todoOcupadojMenuItemActionPerformed

    private void todoLibreMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_todoLibreMenuItemActionPerformed
        CheckMesa1.setSelected(false);
        CheckMesa2.setSelected(false);
        CheckMesa3.setSelected(false);
        CheckMesa4.setSelected(false);
        cambiaColor(Mesa1, CheckMesa1, comedor.posicionMesa(1));
        cambiaColor(Mesa2, CheckMesa2, comedor.posicionMesa(2));
        cambiaColor(Mesa3, CheckMesa3, comedor.posicionMesa(3));
        cambiaColor(Mesa4, CheckMesa4, comedor.posicionMesa(4));
    }//GEN-LAST:event_todoLibreMenuItemActionPerformed

    private void BotonComedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonComedorActionPerformed
        PPrincipal.setVisible(false);
        PanelRest.setVisible(true);
    }//GEN-LAST:event_BotonComedorActionPerformed

    private void jMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu2ActionPerformed
        PPrincipal.setVisible(false);
        PanelRest.setVisible(true);
    }//GEN-LAST:event_jMenu2ActionPerformed

    private void BotonCubiertosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonCubiertosActionPerformed
        LinkedList<Mesa> m = comedor.cargarComedor();
        MesasComboBox.removeAllItems();
        for (int i = 0; i < m.size(); i++) {
            if (!m.get(i).isEstado()) {
                MesasComboBox.addItem(m.get(i).getNombre());
            }
        }

        PPrincipal.setVisible(false);
        PanelRest.setVisible(false);
        PanelComandas.setVisible(true);
    }//GEN-LAST:event_BotonCubiertosActionPerformed

    private void finalizarjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finalizarjButtonActionPerformed
        factura();

    }//GEN-LAST:event_finalizarjButtonActionPerformed

    private void ComedorjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComedorjMenuItemActionPerformed
        PPrincipal.setVisible(false);
        PanelRest.setVisible(true);
        PanelComandas.setVisible(false);
    }//GEN-LAST:event_ComedorjMenuItemActionPerformed

    private void guardarjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarjMenuItemActionPerformed
        comedor.guardar();
    }//GEN-LAST:event_guardarjMenuItemActionPerformed

    private void MesasComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MesasComboBoxActionPerformed
        cambiarmesa((String) MesasComboBox.getSelectedItem());
        jListTotal.setModel(modeloTotal);
    }//GEN-LAST:event_MesasComboBoxActionPerformed

    private void aceptarjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aceptarjButtonActionPerformed
        cargarComanda(p, cambiarmesa((String) MesasComboBox.getSelectedItem()));
        jListOtros.clearSelection();
        jListPrimeros.clearSelection();
        jListSegundos.clearSelection();
    }//GEN-LAST:event_aceptarjButtonActionPerformed

    private void jListPrimerosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListPrimerosValueChanged
        p = jListPrimeros.getSelectedValue();
        jListOtros.clearSelection();
        jListSegundos.clearSelection();
    }//GEN-LAST:event_jListPrimerosValueChanged

    private void jListSegundosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListSegundosValueChanged
        p = jListSegundos.getSelectedValue();
        jListOtros.clearSelection();
        jListPrimeros.clearSelection();
    }//GEN-LAST:event_jListSegundosValueChanged

    private void jListOtrosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListOtrosValueChanged
        p = jListOtros.getSelectedValue();
        jListSegundos.clearSelection();
        jListPrimeros.clearSelection();
    }//GEN-LAST:event_jListOtrosValueChanged

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        borrartodo(false);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        borrar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButtonHacerFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHacerFacturaActionPerformed
        hacerFactura();
        jDialog1.setVisible(false);
        borrartodo(true);
    }//GEN-LAST:event_jButtonHacerFacturaActionPerformed

    private void ComandasjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComandasjMenuItemActionPerformed
         PPrincipal.setVisible(false);
        PanelRest.setVisible(false);
        PanelComandas.setVisible(true);
    }//GEN-LAST:event_ComandasjMenuItemActionPerformed
    private void PantallaMensaje() {
        JOptionPane.showMessageDialog(this, "Daniel Ortega Macho, Adrian Sanchez Rodriguez y Marcos Suarez Illerias", "Acerca de ",
                JOptionPane.PLAIN_MESSAGE, null);
    }

    private void PantallaFinalizarComanda() {

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PaginaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PaginaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PaginaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PaginaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PaginaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem AcercaDe;
    private javax.swing.JButton BotonComedor;
    private javax.swing.JButton BotonCubiertos;
    private javax.swing.JCheckBox CheckMesa1;
    private javax.swing.JCheckBox CheckMesa2;
    private javax.swing.JCheckBox CheckMesa3;
    private javax.swing.JCheckBox CheckMesa4;
    private javax.swing.JLabel Coci;
    private javax.swing.JMenuItem ComandasjMenuItem;
    private javax.swing.JMenuItem ComedorjMenuItem;
    private javax.swing.JLabel Fondo;
    private javax.swing.JLabel Fondo2;
    private javax.swing.JLabel FondoComandas;
    private javax.swing.JList<Comanda> JListaDialog;
    private javax.swing.JMenuItem MenuSalida;
    private javax.swing.JLabel Mesa1;
    private javax.swing.JLabel Mesa2;
    private javax.swing.JLabel Mesa3;
    private javax.swing.JLabel Mesa4;
    private javax.swing.JComboBox<String> MesasComboBox;
    private javax.swing.JPanel PPrincipal;
    private javax.swing.JPanel PanelComandas;
    private javax.swing.JPanel PanelMesa1;
    private javax.swing.JPanel PanelMesa2;
    private javax.swing.JPanel PanelMesa3;
    private javax.swing.JPanel PanelMesa4;
    private javax.swing.JPanel PanelRest;
    private javax.swing.JLabel PrecioLabel;
    private javax.swing.JButton aceptarjButton;
    private javax.swing.JPopupMenu cambiarTodojPopupMenu;
    private javax.swing.JButton finalizarjButton;
    private javax.swing.JMenuItem guardarjMenuItem;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButtonHacerFactura;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelNumeroMesa;
    private javax.swing.JLabel jLabelPrecioDialog;
    private javax.swing.JList<Platos> jListOtros;
    private javax.swing.JList<Platos> jListPrimeros;
    private javax.swing.JList<Platos> jListSegundos;
    private javax.swing.JList<Comanda> jListTotal;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JMenuItem todoLibreMenuItem;
    private javax.swing.JMenuItem todoOcupadojMenuItem;
    // End of variables declaration//GEN-END:variables

}
